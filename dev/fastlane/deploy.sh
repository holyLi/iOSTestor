curl https://rink.hockeyapp.net/api/2/apps/f6c953971576416aaa499358d0cf6112/app_versions \
-F "status=2" \
-F "notes=Some new features and fixed bugs." \
-F "notify=1" \
-F "ipa=@./fastlane/reports/dev.ipa" \
-H "X-HockeyAppToken: 656dd10c7f714c6ea34881e31c97e68b" 